Features:
==========
        Watch the Object-C call stack

Usage:
==========
        1.Be sure theos env is OK!
        2.Use build.sh to install inspective.deb to target machine
        3.Then use installdylib.sh to upload tweak which base on MobileSubstrate
        4.check the [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]/Document/InspectiveC path
        5.tail the *.log
        
More info:
==========
https://github.com/DavidGoldman/InspectiveC<br>
http://bbs.iosre.com/t/inspectivec/1584<br>