#!/bin/sh
ipAddr="10.0.0.28"
inspecPkg=com.golddavid.inspectivec_1.1.0-8_iphoneos-arm-SDK9.2.deb
scp $inspecPkg root@$ipAddr:/tmp.deb
ssh root@$ipAddr "dpkg -i /tmp.deb"
scp root@$ipAddr:/usr/lib/libinspectivec.dylib .
